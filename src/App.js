import React, { useEffect, useState } from "react";
import axios from "axios";
import {
    getFromLocalStorage,
    saveToLocalStorage,
} from "./utils/localStorage/localStorage";

import "./index.css";
import Header from "./Components/Header/Header";
import AllRoutes from "./Routes";

function App() {
    const [store, setStore] = useState([]);
    const [wish, setWish] = useState([]);
    const [cart, setCart] = useState([]);

    const getStore = async () => {
        const { data } = await axios("./store/store.json");
        setStore(data);

        if (!localStorage.cartList) {
            saveToLocalStorage("cartList", []);
        }
        if (!localStorage.wishList) {
            saveToLocalStorage("wishList", []);
        }
        setWish(getFromLocalStorage("wishList"));
        setCart(getFromLocalStorage("cartList"));
    };

    useEffect(() => {
        getStore();
    }, []);

    return (
        <div className="App">
            <Header wish={wish} cart={cart} />
            <AllRoutes
                wish={wish}
                cart={cart}
                store={store}
                setWish={setWish}
                setCart={setCart}
            />
        </div>
    );
}

export default App;
