import styles from "./Header.module.scss";

import React from "react";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";

import { ReactComponent as Cart } from "../../assets/svg/cart.svg";
import { ReactComponent as Wish } from "../../assets/svg/wish.svg";

const Header = ({ wish, cart }) => {

    let counter = 0;
    cart.forEach(({ count }) => counter += count)

    return (
        <section className={styles.header}>
            <div className={styles.container}>
                <div className={styles.buttons}>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/" >
                        <h1>HOME</h1>
                    </NavLink>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/cart-list" >
                        <Cart />
                        <span>{cart ? counter : 0}</span>
                    </NavLink>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/wish-list" >
                        <Wish />
                        <span>{wish ? wish.length : 0}</span>
                    </NavLink>
                </div>
            </div>
        </section>
    );
};

Header.propTypes = {
  wish: PropTypes.array,
  cart: PropTypes.array
};

Header.defaultProps = {
  wish: [],
  cart: []
};

export default Header;
