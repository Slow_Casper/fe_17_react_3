import styles from "./Modal.module.scss";

import React from "react";
import PropTypes from "prop-types";

const Modal = ({ text, closeModal, removeFromCart, isremoveModal }) => {
    const close = (event) => {
        if (event.target === event.currentTarget) closeModal();
    };

    return (
        <div className={styles.background} onClick={close}>
            <div className={styles.modal}>
                <p className={styles.text}>{text}</p>
                {!isremoveModal ? (
                    <button onClick={closeModal}>OK</button>
                ) : (
                    <div className={styles.removeButtons}>
                        <button className={styles.remove} onClick={removeFromCart}>Remove</button>
                        <button onClick={closeModal}>Cancel</button>
                    </div>
                )}
            </div>
        </div>
    );
};

Modal.propTypes = {
    text: PropTypes.string,
    closeModal: PropTypes.func,
};

Modal.defaultProps = {
    text: "Не заданий текст",
    closeModal: () => {},
};

export default Modal;
