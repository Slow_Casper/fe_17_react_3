import styles from "./StoreItem.module.scss";

import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";

import {
    saveToLocalStorage,
    getFromLocalStorage,
} from "../../utils/localStorage/localStorage";

import { ReactComponent as Star } from "../../assets/svg/star.svg";
import { ReactComponent as StarFilled } from "../../assets/svg/star_filled.svg";
import Modal from "../Modal/Modal";
import { useLocation } from "react-router-dom";

const StoreItem = ({ item, setCart, setWish }) => {
    const [isFavor, setIsFavor] = useState(false);
    const [isModal, setIsModal] = useState(false);
    const [isremoveModal, setIsRemoveModal] = useState(false);

    const { id, name, price, url, width, height, article, color } = item;

    const location = useLocation();

    useEffect(() => {
        const wishLS = getFromLocalStorage("wishList");

        const findWish = wishLS
            ? Boolean(wishLS.find((el) => el.id === id))
            : false;

        if (findWish) {
            setIsFavor(true);
        } else {
            setIsFavor(false);
        }
    }, [id]);

    const addToCart = () => {
        setCart((prev) => {
            const fantomState = [...prev];
            const index = fantomState.findIndex((el) => el.id === id);
            console.log(index);
            setIsModal(true);

            if (index !== -1) {
                fantomState[index].count++;
                window.localStorage.removeItem("cartList");
                saveToLocalStorage("cartList", fantomState);
                return fantomState;
            } else {
                item.count++;
                const newState = [item, ...prev];
                console.log(newState);
                saveToLocalStorage("cartList", newState);
                return newState;
            }
        });
    };

    const removeModal = () => {
        setIsRemoveModal(true);
    }

    const removeFromCart = () => {
        setCart((prev) => {
            console.log(prev)
            const fantomState = [...prev];
            const newState = fantomState.filter((el) => el.id !== id)
            setIsRemoveModal(false);
            window.localStorage.removeItem("cartList");
            saveToLocalStorage("cartList", newState);
            return newState;
        });
    };

    const toggleFavor = () => {
        setWish((prev) => {
            const fantomState = [...prev];
            const index = fantomState.find((el) => el.id === id);

            if (index) {
                const newState = fantomState.filter((el) => el.id !== id);
                saveToLocalStorage("wishList", newState);
                setIsFavor(false);
                return newState;
            } else {
                const newState = [item, ...prev];
                saveToLocalStorage("wishList", newState);
                setIsFavor(true);
                return newState;
            }
        });
    };

    const close = () => {
        setIsModal(false);
        setIsRemoveModal(false);
    };

    return (
        <>
            <div className={styles.card}>
                <img src={url} alt="#" width={width} height={height} />
                <p className={styles.article}>{article}</p>
                <h2 className={styles.title}>{name}</h2>
                <div className={styles.description}>
                    <div style={{ backgroundColor: color }}></div>
                    <p>{price}</p>
                </div>
                {location.pathname === "/cart-list" && (
                    <p className={styles.cartCount}>В корзину додано {item.count} шт.</p>
                )}
                <div className={styles.buttons}>
                    <Button
                        classes={styles.favBtn}
                        text={isFavor ? <StarFilled /> : <Star />}
                        onClick={toggleFavor}
                    />
                    {location.pathname === "/cart-list" ? (
                        <Button
                            classes={styles.btn}
                            text="Remove from cart"
                            onClick={removeModal}
                        />
                    ) : (
                        <Button
                            classes={styles.btn}
                            text="Add to cart"
                            onClick={addToCart}
                        />
                    )}
                </div>
            </div>

            {isModal && (
                <Modal text="Товар додано до кошика" closeModal={close} />
            )}

            {isremoveModal && (
                <Modal 
                    text="Ви впевнені, що бажаєте видалити товар з корзини?"
                    removeFromCart={removeFromCart}
                    closeModal={close}
                    isremoveModal={() => isremoveModal}
                />
            )}
        </>
    );
};

StoreItem.propTypes = {
    item: PropTypes.object,
    setCart: PropTypes.func,
    setWish: PropTypes.func,
};

StoreItem.defaultProps = {
    item: {},
    setCart: () => {},
    setWish: () => {},
};

export default StoreItem;
