import StoreItem from "../StoreItem/StoreItem";
import styles from "./StoreList.module.scss";
import PropTypes from "prop-types"



const StoreList = ({ store, setCart, setWish }) => {

    return (
        <section className={styles.store}>
            <div className={styles.container}>
                {store.map((item) => {
                    return (
                        <StoreItem key={item.id} item={item} setCart={setCart} setWish={setWish} />
                    )
                })}
            </div>
        </section>
    )
}

StoreList.propTypes = {
    store: PropTypes.array,
    setCart: PropTypes.func,
    setWish: PropTypes.func
}

StoreList.defaultProps = {
    store: [],
    setCart: () => {},
    setWish: () => {}
}

export default StoreList