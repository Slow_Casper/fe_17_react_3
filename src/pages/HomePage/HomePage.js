import React from "react";
import PropTypes from "prop-types";
import StoreList from "../../Components/StoreList/StoreList";

const Home = ({ store, setWish, setCart }) => {

    return (
        <>
            <StoreList store={store} setWish={setWish} setCart={setCart} />
        </>
    );
};

Home.propTypes = {
    store: PropTypes.array,
    setWish: PropTypes.func,
    setCart: PropTypes.func
};

Home.defaultProps = {
    store: [],
    setWish: () => {},
    setCart: () => {}
};

export default Home;
