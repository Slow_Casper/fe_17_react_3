import React from 'react'
import PropTypes from 'prop-types'
import StoreList from "../../Components/StoreList/StoreList"


const WishPage = ({ wish, setWish, setCart }) => {
    return (
        <StoreList store={wish} setWish={setWish} setCart={setCart} />
      )
}

WishPage.propTypes = {
    wish: PropTypes.array,
    setWish: PropTypes.func
}

WishPage.defaultProps ={
    wish: [],
    setWish: () => {}
}

export default WishPage