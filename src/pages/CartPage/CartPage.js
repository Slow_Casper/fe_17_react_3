import React from 'react'
import PropTypes from 'prop-types'
import StoreList from "../../Components/StoreList/StoreList"

const CartPage = ({ cart, setCart, setWish }) => {
  return (
    <StoreList store={cart} setWish={setWish} setCart={setCart} />
  )
}

CartPage.propTypes = {
    cart: PropTypes.array,
    setCart: PropTypes.func
}

CartPage.defaultProps = {
    cart: [],
    setCart: () => {}
}

export default CartPage