import { Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import CartPage from './pages/CartPage/CartPage';
import WishPage from './pages/WishPage/WishPage';


const AllRoutes = ({ wish, cart, store, setWish, setCart }) => {

    return(
        <Routes>
            <Route path="/" element={<HomePage store={store} setWish={setWish} setCart={setCart} />} />
            <Route path="/cart-list" element={<CartPage cart={cart} setCart={setCart} setWish={setWish} />} />
            <Route path="/wish-list" element={<WishPage wish={wish} setCart={setCart} setWish={setWish} />} />
        </Routes>
    )
}

export default AllRoutes